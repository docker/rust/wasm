FROM rust

LABEL maintainer="Elois Librelois"
LABEL version="1.51"
LABEL description="CI for rust-wasm projects"

# add target wasm32-unknown-unknown
RUN rustup target add wasm32-unknown-unknown

# install wasm-pack
RUN cargo  install wasm-pack

# install wasm-gc
RUN cargo install wasm-gc
